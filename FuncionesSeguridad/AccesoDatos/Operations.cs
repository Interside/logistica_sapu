﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class Operations
    {
        private SqlConnection conexionSQL;
        private SqlTransaction transaccionSQL;


        public DataTable EjecutarSPSQL(string sp, object parametros, bool retorna, string bd, bool tran = false)
        {
            try
            {
                if (!tran)
                {
                    ConectarSQL(bd);
                }

                DataTable res = new DataTable();
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();

                if (tran)
                {
                    cmd.Transaction = transaccionSQL;
                }

                cmd.Connection = conexionSQL;
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = sp;

                if (parametros != null)
                {
                    Type t = parametros.GetType();
                    PropertyInfo[] lp = t.GetProperties();

                    foreach (PropertyInfo p in lp)
                    {
                        SqlParameter op = new SqlParameter();
                        op.ParameterName = p.Name;
                        op.SqlDbType = selectorTipoSQL(p.GetValue(parametros, null));
                        op.Value = p.GetValue(parametros, null);

                        cmd.Parameters.Add(op);
                    }

                }

                if (retorna)
                {
                    da.SelectCommand = cmd;

                    da.Fill(res);

                    if (!tran)
                    {
                        DesconectarSQL();
                    }
                    return res;
                }
                else
                {
                    cmd.ExecuteNonQuery();

                    if (!tran)
                    {
                        DesconectarSQL();
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (conexionSQL.State == ConnectionState.Open)
                {
                    conexionSQL.Close();
                }
            }
        }


        public bool ConectarSQL(string bd)
        {
            try
            {
                conexionSQL = Connection.ConectaSQL(bd);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DesconectarSQL()
        {
            try
            {
                conexionSQL.Close();


                return true;
            }
            catch
            {
                return false;
            }
        }



        private SqlDbType selectorTipoSQL(object p)
        {
            SqlDbType ret;
            if (p.GetType() == Type.GetType("System.Int32"))
            {
                return SqlDbType.Int;
            }
            else if (p.GetType() == Type.GetType("System.String"))
            {
                return SqlDbType.VarChar;
            }
            else if (p.GetType() == Type.GetType("System.Single"))
            {
                return SqlDbType.Real;
            }
            else if (p.GetType() == Type.GetType("System.Double"))
            {
                return SqlDbType.Float;
            }
            else if (p.GetType() == Type.GetType("System.Boolean"))
            {
                return SqlDbType.Bit;
            }
            else if (p.GetType() == Type.GetType("System.DateTime"))
            {
                return SqlDbType.DateTime;
            }
            else if (p.GetType() == Type.GetType("System.Decimal"))
            {
                return SqlDbType.Decimal;
            }
            else
            {
                return SqlDbType.VarChar;
            }
        }


    }
}

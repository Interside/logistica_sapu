﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class Connection
    {

        public static SqlConnection ConectaSQL(string bd)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings[bd].ConnectionString;
                con.Open();
                return con;
            }
            catch (Exception)
            {
                throw;
            }
        }



    }
}
